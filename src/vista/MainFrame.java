
package vista;

import controlador.PersonaControlador;
import java.util.Date;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import misc.DateOperator;
import misc.Validator;
import modelo.Persona;

public class MainFrame extends javax.swing.JFrame {
    PersonaControlador pc;
   
    public MainFrame() {
        initComponents();
        setController();
        
    }
    
    private void setController(){
        pc = new PersonaControlador(this);
        pc.setDepartmentComboBox(departmentComboBox);
        pc.setMunicipalityComboBox(municipalityComboBox);
        this.departmentComboBox.addActionListener(pc);
        this.municipalityComboBox.addActionListener(pc);       
       
        this.SelectB.addActionListener(pc);
        this.SaveB.addActionListener(pc);
        this.ClearB.addActionListener(pc);
        this.frmTFID.addFocusListener(pc);
    }
    
    public JFormattedTextField getFrmTFID() {
        return frmTFID;
    }

    public void setFecha(String birthDate, int age) {
        BirthDateTF.setText(birthDate);
        AgeTF.setText(""+age+" años");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JLabel1 = new javax.swing.JLabel();
        JLabel2 = new javax.swing.JLabel();
        JLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        FirstNameTF = new javax.swing.JTextField();
        LastName1TF = new javax.swing.JTextField();
        SecondNameTF = new javax.swing.JTextField();
        LastName2TF = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        AgeTF = new javax.swing.JTextField();
        BirthDateTF = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        departmentComboBox = new javax.swing.JComboBox<>();
        municipalityComboBox = new javax.swing.JComboBox<>();
        jSeparator3 = new javax.swing.JSeparator();
        SelectB = new javax.swing.JButton();
        ClearB = new javax.swing.JButton();
        SaveB = new javax.swing.JButton();
        frmTFID = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        JLabel1.setText("Primer nombre");

        JLabel2.setText("Primer apellido");

        JLabel3.setText("Segundo nombre");

        jLabel4.setText("Segundo apellido");

        jLabel5.setText("# de cédula");

        jLabel6.setText("Edad");

        jLabel7.setText("Fecha de nacimiento");

        jLabel8.setText("Departamento");

        jLabel9.setText("Municipio");

        departmentComboBox.setActionCommand("department");
        departmentComboBox.setName("department"); // NOI18N

        municipalityComboBox.setActionCommand("municipality");
        municipalityComboBox.setName("municipality"); // NOI18N

        SelectB.setText("Seleccionar");
        SelectB.setActionCommand("select");

        ClearB.setText("Limpiar");
        ClearB.setActionCommand("clear");

        SaveB.setText("Guardar");
        SaveB.setActionCommand("save");

        try {
            frmTFID.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-######-####U")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        frmTFID.setName("frmTFID"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(SelectB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ClearB)
                        .addGap(36, 36, 36)
                        .addComponent(SaveB)
                        .addGap(80, 80, 80))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(JLabel2)
                                        .addGap(18, 18, 18)
                                        .addComponent(LastName1TF))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(JLabel1)
                                        .addGap(18, 18, 18)
                                        .addComponent(FirstNameTF, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JLabel3)
                                    .addComponent(jLabel4))
                                .addGap(29, 29, 29)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(SecondNameTF, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
                                    .addComponent(LastName2TF)))
                            .addComponent(jSeparator2)
                            .addComponent(jSeparator3)
                            .addComponent(jSeparator1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(18, 18, 18)
                                        .addComponent(AgeTF, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(18, 18, 18)
                                        .addComponent(frmTFID, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BirthDateTF, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(49, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(departmentComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel9)
                        .addGap(18, 18, 18)
                        .addComponent(municipalityComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JLabel1)
                    .addComponent(JLabel3)
                    .addComponent(FirstNameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SecondNameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JLabel2)
                    .addComponent(jLabel4)
                    .addComponent(LastName1TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LastName2TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7)
                    .addComponent(BirthDateTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(frmTFID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(AgeTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(departmentComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(municipalityComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SelectB)
                    .addComponent(ClearB)
                    .addComponent(SaveB))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /*private void IdTFFocusLost(java.awt.event.FocusEvent evt) {                                      
        if (Validator.isNicaraguanNumberId(frmTFID.getText())){
            String birthDate = Validator.getBirthDateFromId(frmTFID.getText());
            BirthDateTF.setText(birthDate);
            Date bd = DateOperator.stringToDate(birthDate, "yyyy-MM-dd");        
            AgeTF.setText(String.valueOf(DateOperator.getAge(bd)));
        }
        else
        {
            JOptionPane.showMessageDialog(this, "El no. de cédula proporcionado no es válido", this.getTitle(), JOptionPane.WARNING_MESSAGE);
            frmTFID.requestFocus();
            frmTFID.selectAll();
        }
    }*/                                     
    
    public void setPersonData(Persona p){
        FirstNameTF.setText(p.getFirstName());
        SecondNameTF.setText(p.getSecondName());
        LastName1TF.setText(p.getLastName1());
        LastName2TF.setText(p.getLastName2());
        frmTFID.setText(p.getId());
        BirthDateTF.setText(DateOperator.dateToString(p.getBirthDate(),"yyyy-MM-dd"));
        AgeTF.setText("");
    }
    public Persona getPersonData(){
        Persona p = new Persona();
        p.setFirstName(FirstNameTF.getText());
        p.setSecondName(SecondNameTF.getText());
        p.setLastName1(LastName1TF.getText());
        p.setLastName2(LastName2TF.getText());
        p.setId(frmTFID.getText());
        p.setBirthDate(DateOperator.stringToDate(BirthDateTF.getText(), "yyyy-MM-dd"));
        p.setAge(Integer.parseInt(AgeTF.getText()));
        return p;
    }
    public void clear(){
        FirstNameTF.setText("");
        SecondNameTF.setText("");
        LastName1TF.setText("");
        LastName2TF.setText("");
        frmTFID.setText("");
        BirthDateTF.setText("");
        AgeTF.setText("");
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField AgeTF;
    private javax.swing.JTextField BirthDateTF;
    private javax.swing.JButton ClearB;
    private javax.swing.JTextField FirstNameTF;
    private javax.swing.JLabel JLabel1;
    private javax.swing.JLabel JLabel2;
    private javax.swing.JLabel JLabel3;
    private javax.swing.JTextField LastName1TF;
    private javax.swing.JTextField LastName2TF;
    private javax.swing.JButton SaveB;
    private javax.swing.JTextField SecondNameTF;
    private javax.swing.JButton SelectB;
    private javax.swing.JComboBox<String> departmentComboBox;
    private javax.swing.JFormattedTextField frmTFID;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JComboBox<String> municipalityComboBox;
    // End of variables declaration//GEN-END:variables
}
